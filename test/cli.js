const { test } = require('blue-tape')
const { join } = require('path')
const { spawn } = require('child_process')
const {
  createReadStream,
  promises: {
    readdir,
    readFile
  }
} = require('fs')

test('Check all the fixtures', async (t) => {
  const fixtures = join(__dirname, 'fixtures')
  for (const fixture of await readdir(fixtures)) {
    test(`Fixture: ${fixture.replace(/-/g, ' ')}`, async (t) => {
      const expected = await readFile(
        join(fixtures, fixture, 'expected.csv'),
        { encoding: 'utf8' }
      )
      const given = createReadStream(join(fixtures, fixture, 'given.json'))
      const child = spawn(
        process.execPath,
        [join(__dirname, '../index.js')],
        { stdio: ['pipe', 'pipe', 'inherit'] }
      )
      given.pipe(child.stdin)
      const chunks = []
      for await (const chunk of child.stdout) {
        chunks.push(chunk)
      }
      const actual = Buffer.concat(chunks).toString()
      t.is(actual, expected)
    })
  }
})
