#!/usr/bin/env node

const readline = require('readline')

function escapeCsv (field) {
  const stringified = String(field)
  const escaped = stringified.replace(/"/g, '""')
  const containsComma = stringified.indexOf(',') !== -1
  const containsQuotes = stringified.length !== escaped.length
  if (containsComma || containsQuotes) {
    return `"${escaped}"`
  } else {
    return escaped
  }
}

function writeCsv (list) {
  const csv = list.map(escapeCsv).join(',') + '\n'
  process.stdout.write(csv)
}

function isStringable (value) {
  return typeof value === 'string' ||
    typeof value === 'number' ||
    typeof value === 'boolean' ||
    value === null
}

async function main () {
  const trailingCommas = /,$/
  let headers
  const linereader = readline.createInterface({ input: process.stdin })
  for await (const line of linereader) {
    const trimmed = line.trim()
    let row
    try {
      row = JSON.parse(trimmed.replace(trailingCommas, ''))
    } catch (error) {
      continue
    }
    if (headers === undefined) {
      headers = Object.keys(row)
      writeCsv(headers)
    }
    const columns = headers.map((header) => {
      return isStringable(row[header])
        ? row[header] : ''
    })
    writeCsv(columns)
  }
}

main()
