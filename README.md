# ndjson2csv

CLI tool that turns NDJSON into CSV.

Ignores lines that do not parse as JSON.

Ignores trailing commas so it also works with regular JSON.

JSON properties of the first valid row become the CSV headers column names.

Compatible with JSONL or JSON lines format.

## Usage

```bash
cat metrics.json | npx ndjson2csv > spreadsheet.csv
```
